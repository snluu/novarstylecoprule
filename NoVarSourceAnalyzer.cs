﻿// <copyright file="NoVarSourceAnalyzer.cs" company="Steven Luu">Copyright (c) Steven Luu</copyright>

namespace NoVarStyleCopRule
{
    using StyleCop;
    using StyleCop.CSharp;

    /// <summary>
    /// Provides a C# code analyzer to prevent the usage of the <code>var</code> keyword
    /// </summary>
    [SourceAnalyzer(typeof(CsParser))]
    public class NoVarSourceAnalyzer : SourceAnalyzer
    {
        /// <summary>
        /// Analyzes the code document
        /// </summary>
        /// <param name="document">The code document</param>
        public override void AnalyzeDocument(CodeDocument document)
        {
            CsDocument csharpDoc = document as CsDocument;

            if (csharpDoc == null || csharpDoc.RootElement == null || csharpDoc.RootElement.Generated)
            {
                return;
            }

            csharpDoc.WalkDocument(
                new CodeWalkerElementVisitor<NoVarSourceAnalyzer>(this.VisitElement),
                context: this);
        }
        
        /// <summary>
        /// Visit a code element
        /// </summary>
        /// <param name="element">The code element</param>
        /// <param name="parentElement">The parent of this code element</param>
        /// <param name="context">The source analyzer</param>
        /// <returns>If the element was successfully visited</returns>
        private bool VisitElement(CsElement element, CsElement parentElement, NoVarSourceAnalyzer context)
        {
            if (element.Generated)
            {
                return true;
            }

            if (element.ElementType == ElementType.Method || element.ElementType == ElementType.Constructor || element.ElementType == ElementType.Property)
            {
                foreach (CsToken token in element.Tokens)
                {
                    if (token.Text == "var")
                    {
                        this.AddViolation(element, token.LineNumber, ruleName: "NoVarInDeclaration");
                    }
                }
            }

            return true;
        }
    }
}
